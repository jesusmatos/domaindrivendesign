CREATE TABLE `habitacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_habitacion` varchar(10) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `id_tipo_habitacion` int(11) NOT NULL,
  `disponible` tinyint(4) NOT NULL,
  `estado` int(11) NOT NULL,
  `url_foto` varchar(250) DEFAULT NULL,
  `detalles` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_habitacion_fk_idx` (`id_tipo_habitacion`),
  CONSTRAINT `id_tipo_habitacion_fk` FOREIGN KEY (`id_tipo_habitacion`) REFERENCES `tipo_habitacion` (`id_tipo_habitacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;