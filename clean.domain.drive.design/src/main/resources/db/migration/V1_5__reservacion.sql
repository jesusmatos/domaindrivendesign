CREATE TABLE `reservacion` (
  `habitacion_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `fecha_reservacion` datetime NOT NULL,
  `checking` date NOT NULL,
  `checkout` date NOT NULL,
  `cantidad_huespedes` int(11) NOT NULL,
  `estado_reservacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`habitacion_id`,`cliente_id`,`id`),
  KEY `fk_habitacion_has_cliente_cliente1_idx` (`cliente_id`),
  KEY `fk_habitacion_has_cliente_habitacion_idx` (`habitacion_id`),
  CONSTRAINT `fk_habitacion_has_cliente_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_habitacion_has_cliente_habitacion` FOREIGN KEY (`habitacion_id`) REFERENCES `habitacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;