CREATE TABLE `tipo_habitacion` (
  `id_tipo_habitacion` int(11) NOT NULL,
  `descripcion_tipo_habitacion` varchar(80) NOT NULL,
  `precio` double NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `detalle_servicio` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_habitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;