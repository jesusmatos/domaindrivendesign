package pe.edu.unmsm.dyp.hotel.domain.exceptions;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
@SuppressWarnings("serial")
public class HabitacionNoDisponibleException extends Exception{

	public HabitacionNoDisponibleException(){
		super("La habitación seleccionada actualmente no cuenta con disponibilidad");
	}
	
}
