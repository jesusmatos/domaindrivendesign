package pe.edu.unmsm.dyp.hotel.domain.repository;

import pe.edu.unmsm.dyp.hotel.domain.model.Cliente;

public interface ClienteRepository extends GenericRepository<Cliente>{

}
