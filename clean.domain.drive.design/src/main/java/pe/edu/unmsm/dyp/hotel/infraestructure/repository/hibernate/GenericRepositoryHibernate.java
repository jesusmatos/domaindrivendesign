package pe.edu.unmsm.dyp.hotel.infraestructure.repository.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pe.edu.unmsm.dyp.hotel.domain.repository.GenericRepository;

public abstract class GenericRepositoryHibernate<T> implements GenericRepository<T> {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void save(T entity){
		getSession().save(entity);
	}
	
	@Override
	public void update(T entity){
		getSession().update(entity);
	}
	
	@Override
	public void merge(T entity){
		getSession().merge(entity);
	}
	
}
