package pe.edu.unmsm.dyp.hotel.domain.repository;

import java.util.List;

public interface GenericRepository<T> {

	public void save(T entity);
	
	public void update(T entity);
	
	public void merge(T entity);
	
}
