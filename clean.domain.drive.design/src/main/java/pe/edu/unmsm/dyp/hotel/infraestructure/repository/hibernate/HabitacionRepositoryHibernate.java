package pe.edu.unmsm.dyp.hotel.infraestructure.repository.hibernate;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pe.edu.unmsm.dyp.hotel.application.dto.InputBusquedaHabitacionDto;
import pe.edu.unmsm.dyp.hotel.domain.exceptions.HabitacionNoDisponibleException;
import pe.edu.unmsm.dyp.hotel.domain.model.Habitacion;
import pe.edu.unmsm.dyp.hotel.domain.repository.HabitacionRepository;
import pe.edu.unmsm.dyp.hotel.shared.domain.EstadoType;

@Repository
public class HabitacionRepositoryHibernate extends GenericRepositoryHibernate<Habitacion>
		implements HabitacionRepository{

	@Override
	public List<Habitacion> findHabitacionByIds(List<Integer> ids, int cantidadHuespedes) throws HabitacionNoDisponibleException, SQLException{
		// TODO Auto-generated method stub
		Criteria criteria = getSession().createCriteria(Habitacion.class);
		criteria.add(Restrictions.not(
				Restrictions.in("id", ids)));
		criteria.add(Restrictions.eq("capacidad", cantidadHuespedes));
		List<Habitacion> list = criteria.list();
		if(list == null) 
			throw new HabitacionNoDisponibleException();
		return list;
	}
		
}
