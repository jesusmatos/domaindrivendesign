package pe.edu.unmsm.dyp.hotel.application.dto;

import java.io.Serializable;
import java.util.Date;

public class InputBusquedaHabitacionDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date checking;
	private Date checkout;
	private Integer numeroHabitaciones;
	private Integer cantidadAdultos;
	private Integer cantidadMenores;
	public Date getChecking() {
		return checking;
	}
	public void setChecking(Date checking) {
		this.checking = checking;
	}
	public Date getCheckout() {
		return checkout;
	}
	public void setCheckout(Date checkout) {
		this.checkout = checkout;
	}
	public Integer getNumeroHabitaciones() {
		return numeroHabitaciones;
	}
	public void setNumeroHabitaciones(Integer numeroHabitaciones) {
		this.numeroHabitaciones = numeroHabitaciones;
	}
	public Integer getCantidadAdultos() {
		return cantidadAdultos;
	}
	public void setCantidadAdultos(Integer cantidadAdultos) {
		this.cantidadAdultos = cantidadAdultos;
	}
	public Integer getCantidadMenores() {
		return cantidadMenores;
	}
	public void setCantidadMenores(Integer cantidadMenores) {
		this.cantidadMenores = cantidadMenores;
	}
	
	
}
