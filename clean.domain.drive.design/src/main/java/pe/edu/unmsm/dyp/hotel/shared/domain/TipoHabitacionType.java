package pe.edu.unmsm.dyp.hotel.shared.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
public enum TipoHabitacionType {

	MATRIMONIAL(1 , "MATRIMONIAL SUPERIOR"),
	SIMPLE(2, "HABITACION SIMPLE"),
	DOBLE(3, "HABITACION DOBLE");
	
	private final Integer codigo;
	private final String descripcion;
	private static Map<Integer, String> mMap;
	
	private TipoHabitacionType(Integer codigo, String descripcion){
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public static String getDescripcionByCodigo(String codigo){
		if(mMap == null){
			initializeMapping();
		}
		if(mMap.containsKey(codigo)){
			return mMap.get(codigo);
		}
		return null;
	}
	
	private static void initializeMapping(){
		mMap = new HashMap<Integer, String>();
		for(TipoHabitacionType s:TipoHabitacionType.values()){
			mMap.put(s.codigo, s.descripcion);
		}
	}
	
}
