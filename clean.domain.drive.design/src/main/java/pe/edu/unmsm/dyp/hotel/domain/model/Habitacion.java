package pe.edu.unmsm.dyp.hotel.domain.model;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
public class Habitacion {

	private Integer id;
	private String  numeroHabitacion;
	private Integer capacidad;
	private boolean disponible;
	private Integer estado;
	private TipoHabitacion tipoHabitacion;
	private String urlFoto;
	private String detalles;
	
	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}

	public Habitacion(){ }	
	
	// Logica cuando se realiza el checking
	public void checkingHabitacion(){
		if(!disponible)
			this.disponible = true;
	}
	
	// Logica al retirarse el cliente
	public void checkoutHabitacion(){
		if(disponible)
			this.disponible = false;
	}
	
	public Habitacion(Integer id, String numeroHabitacion, Integer capacidad) {
		this.id = id;
		this.numeroHabitacion = numeroHabitacion;
		this.capacidad = capacidad;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNumeroHabitacion() {
		return numeroHabitacion;
	}
	public void setNumeroHabitacion(String numeroHabitacion) {
		this.numeroHabitacion = numeroHabitacion;
	}
	public Integer getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public TipoHabitacion getTipoHabitacion() {
		return tipoHabitacion;
	}

	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}	
	
}
