package pe.edu.unmsm.dyp.hotel.application.dto;

public class DestinoDto {

	private Integer id;
	private String nombre;
	
	public DestinoDto(){ }
	
	public DestinoDto(Integer id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
