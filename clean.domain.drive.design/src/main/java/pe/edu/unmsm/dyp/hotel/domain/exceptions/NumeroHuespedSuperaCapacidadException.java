package pe.edu.unmsm.dyp.hotel.domain.exceptions;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
@SuppressWarnings("serial")
public class NumeroHuespedSuperaCapacidadException extends Exception{

	public NumeroHuespedSuperaCapacidadException(){
		super("El numero de Huespedes que desea registrar Supera la capacidad permitida para esta habitación");
	}
	
}
