package pe.edu.unmsm.dyp.hotel.domain.repository;

import pe.edu.unmsm.dyp.hotel.domain.model.TipoHabitacion;

public interface TipoHabitacionRepository extends GenericRepository<TipoHabitacion> {

}
