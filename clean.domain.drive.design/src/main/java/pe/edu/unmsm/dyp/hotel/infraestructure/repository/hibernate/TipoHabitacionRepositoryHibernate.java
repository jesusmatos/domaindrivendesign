package pe.edu.unmsm.dyp.hotel.infraestructure.repository.hibernate;

import org.springframework.stereotype.Repository;

import pe.edu.unmsm.dyp.hotel.domain.model.TipoHabitacion;
import pe.edu.unmsm.dyp.hotel.domain.repository.TipoHabitacionRepository;

@Repository
public class TipoHabitacionRepositoryHibernate extends GenericRepositoryHibernate<TipoHabitacion> 
																implements TipoHabitacionRepository{

}
