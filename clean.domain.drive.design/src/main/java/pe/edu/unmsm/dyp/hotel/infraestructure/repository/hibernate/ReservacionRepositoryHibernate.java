package pe.edu.unmsm.dyp.hotel.infraestructure.repository.hibernate;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import pe.edu.unmsm.dyp.hotel.domain.model.Reservacion;
import pe.edu.unmsm.dyp.hotel.domain.repository.ReservacionRepository;

@Repository
public class ReservacionRepositoryHibernate extends GenericRepositoryHibernate<Reservacion>
					implements ReservacionRepository{

	@Override
	public List<Reservacion> validarDisponibilidadHabitacionByReservacion(Date checking, Date checkout)
			throws SQLException {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer();
		sql.append("from Reservacion");
		sql.append(" where ( (checking <= :fechaInicio and checkout >= :fechaFin) ");
		sql.append(" or checkout between :fechaInicio and :fechaFin ");
		sql.append(" or checking between :fechaInicio and :fechaFin )");
		Query query = getSession().createQuery(sql.toString());
		query.setParameter("fechaInicio", checking );
		query.setParameter("fechaFin", checkout);
		List<Reservacion> list = query.list();
		return list;
	}

}
