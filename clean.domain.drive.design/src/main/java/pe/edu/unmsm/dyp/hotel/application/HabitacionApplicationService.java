package pe.edu.unmsm.dyp.hotel.application;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.edu.unmsm.dyp.hotel.application.dto.HabitacionDto;
import pe.edu.unmsm.dyp.hotel.application.dto.InputBusquedaHabitacionDto;
import pe.edu.unmsm.dyp.hotel.domain.exceptions.HabitacionNoDisponibleException;
import pe.edu.unmsm.dyp.hotel.domain.model.Habitacion;
import pe.edu.unmsm.dyp.hotel.domain.model.Reservacion;
import pe.edu.unmsm.dyp.hotel.domain.repository.HabitacionRepository;
import pe.edu.unmsm.dyp.hotel.domain.repository.ReservacionRepository;

@Service
public class HabitacionApplicationService {

	@Autowired
	private HabitacionRepository habitacionRepository;
	
	@Autowired
	private ReservacionRepository reservacionRepository;
	
	@Transactional
	public List<HabitacionDto> findAvailabilityRoom(InputBusquedaHabitacionDto param) throws SQLException, HabitacionNoDisponibleException, Exception{
		List<Reservacion> reservaciones = reservacionRepository.validarDisponibilidadHabitacionByReservacion(param.getChecking(), param.getCheckout());
		List<Integer> id_habitaciones = new ArrayList<>();
		for(Reservacion reservacion:reservaciones){
			id_habitaciones.add(reservacion.getIdHabitacion());			
		}
		int totalHuespedes = param.getCantidadAdultos() + param.getCantidadMenores();
		List<Habitacion> habitaciones = habitacionRepository.findHabitacionByIds(id_habitaciones, totalHuespedes);
		List<HabitacionDto> listHabitacionDTO = new ArrayList();
		for(Habitacion habitacion:habitaciones){
			HabitacionDto habitacionDTO = new HabitacionDto();
			BeanUtils.copyProperties(habitacionDTO, habitacion);
			listHabitacionDTO.add(habitacionDTO);
		}
		return listHabitacionDTO;
	}
}
