package pe.edu.unmsm.dyp.hotel.application.dto;

import java.io.Serializable;
import java.util.List;

public class OutputBusquedaHabitacionDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private List<HabitacionDto> habitacionDTO;
	public List<HabitacionDto> getHabitacionDTO() {
		return habitacionDTO;
	}
	public void setHabitacionDTO(List<HabitacionDto> habitacionDTO) {
		this.habitacionDTO = habitacionDTO;
	}
	private String status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
