package pe.edu.unmsm.dyp.hotel.web.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.unmsm.dyp.hotel.application.HabitacionApplicationService;
import pe.edu.unmsm.dyp.hotel.application.dto.HabitacionDto;
import pe.edu.unmsm.dyp.hotel.application.dto.InputBusquedaHabitacionDto;
import pe.edu.unmsm.dyp.hotel.application.dto.OutputBusquedaHabitacionDto;
import pe.edu.unmsm.dyp.hotel.domain.exceptions.HabitacionNoDisponibleException;

@RestController
@RequestMapping("api/booking/")
public class ReservacionController {

	@Autowired
	private HabitacionApplicationService habitacionServices;
	
	@PostMapping("buscarHoteles/")
	public OutputBusquedaHabitacionDto getConsultaHoteles(@RequestBody InputBusquedaHabitacionDto param){
		List<HabitacionDto> listaHabitacionesDisponibles = null;
		OutputBusquedaHabitacionDto output = new OutputBusquedaHabitacionDto();
		try {
			listaHabitacionesDisponibles = habitacionServices.findAvailabilityRoom(param);
			output.setHabitacionDTO(listaHabitacionesDisponibles);
			output.setMessage("Operación Ejecutada con éxito");
			output.setStatus(HttpStatus.OK.name());
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (HabitacionNoDisponibleException e) {
			output.setMessage("No Hay Habitaciones Disponibles");
			output.setStatus(HttpStatus.OK.name());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return output;
	}
	
}
