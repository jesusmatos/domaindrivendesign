package pe.edu.unmsm.dyp.hotel.shared.domain;

public class Error {

	private String mensaje;
	private Exception excepcion;
	
	public Error() {	}
	
	public Error(String mensaje, Exception excepcion) {
		super();
		this.mensaje = mensaje;
		this.excepcion = excepcion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Exception getExcepcion() {
		return excepcion;
	}
	public void setExcepcion(Exception excepcion) {
		this.excepcion = excepcion;
	}
	
	
}
