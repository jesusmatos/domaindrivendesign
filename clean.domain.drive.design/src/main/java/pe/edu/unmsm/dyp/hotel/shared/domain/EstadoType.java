package pe.edu.unmsm.dyp.hotel.shared.domain;

import java.util.HashMap;
import java.util.Map;

public enum EstadoType {
	ACTIVO(1, "ACTIVO"),
	INACTIVO(0, "INACTIVO");
	
	private final Integer codigo;
	private final String descripcion;
	private static Map<Integer, String> mMap;
	
	private EstadoType(Integer codigo, String descripcion){
		this.codigo = codigo;
		this.descripcion = descripcion;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public static String getDescripcionByCodigo(String codigo){
		if(mMap == null){
			initializeMapping();
		}
		if(mMap.containsKey(codigo)){
			return mMap.get(codigo);
		}
		return null;
	}
	
	private static void initializeMapping(){
		mMap = new HashMap<Integer, String>();
		for(EstadoType s:EstadoType.values()){
			mMap.put(s.codigo, s.descripcion);
		}
	}
}
