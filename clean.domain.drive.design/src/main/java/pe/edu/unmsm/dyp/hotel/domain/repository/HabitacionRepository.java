package pe.edu.unmsm.dyp.hotel.domain.repository;

import java.sql.SQLException;
import java.util.List;
import pe.edu.unmsm.dyp.hotel.domain.exceptions.HabitacionNoDisponibleException;
import pe.edu.unmsm.dyp.hotel.domain.model.Habitacion;

public interface HabitacionRepository extends GenericRepository<Habitacion>{
	
	public List<Habitacion> findHabitacionByIds(List<Integer> ids, 
			int cantidadHuespedes) throws HabitacionNoDisponibleException, SQLException;;
}
