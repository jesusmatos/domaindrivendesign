package pe.edu.unmsm.dyp.hotel.application.dto;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
public class ClienteDto {

	private Integer id;
	private String nombres;
	private String apellidos;
	private String email;
	private String celular;
	private String nombreCompleto;
	
	public ClienteDto(){ }
	
	public ClienteDto(Integer id, String nombres, String apellidos, String email, String celular,
			String nombreCompleto) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.email = email;
		this.celular = celular;
		this.nombreCompleto = nombreCompleto;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
}
