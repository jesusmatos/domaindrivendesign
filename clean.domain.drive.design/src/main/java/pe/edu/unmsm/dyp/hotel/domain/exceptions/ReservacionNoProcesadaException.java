package pe.edu.unmsm.dyp.hotel.domain.exceptions;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
@SuppressWarnings("serial")
public class ReservacionNoProcesadaException extends Exception {

	public ReservacionNoProcesadaException(){
		super("Ocurrió un Error inesperado durante la reservación de su habitación");
	}
}
