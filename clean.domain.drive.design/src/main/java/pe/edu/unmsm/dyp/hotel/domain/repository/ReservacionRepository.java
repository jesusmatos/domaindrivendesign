package pe.edu.unmsm.dyp.hotel.domain.repository;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import pe.edu.unmsm.dyp.hotel.domain.model.Reservacion;

public interface ReservacionRepository extends GenericRepository<Reservacion>{

	public List<Reservacion> validarDisponibilidadHabitacionByReservacion(Date checking, Date checkout)
							throws SQLException;
	
}
