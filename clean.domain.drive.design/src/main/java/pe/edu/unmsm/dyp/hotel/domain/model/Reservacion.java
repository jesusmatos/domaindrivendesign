package pe.edu.unmsm.dyp.hotel.domain.model;

import java.util.Date;

import pe.edu.unmsm.dyp.hotel.domain.exceptions.HabitacionNoDisponibleException;
import pe.edu.unmsm.dyp.hotel.domain.exceptions.NumeroHuespedSuperaCapacidadException;

public class Reservacion {
	
	private Integer id;
	private Date fechaReservacion;
	private Date checking;
	private Date checkout;
	private Integer idCliente;
	private Integer idHabitacion;
	private Integer cantidadHuespedes;
	private Integer estadoReservacion;
	
	public Reservacion() { }
	
	public int generarCodigoReservacion() {
		return (int) (Math.random() * 5) + 1;
	}

	public Habitacion reservaHabitacion(Habitacion habitacion) throws HabitacionNoDisponibleException{
		if(!habitacion.isDisponible())	
			throw new HabitacionNoDisponibleException();
		habitacion.setDisponible(false);
		return habitacion;
	}
	
	public void validarCapacidadHabitacion(Habitacion habitacion) throws NumeroHuespedSuperaCapacidadException {
		if(habitacion.getCapacidad() < cantidadHuespedes)
			throw new NumeroHuespedSuperaCapacidadException();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getFechaReservacion() {
		return fechaReservacion;
	}
	public void setFechaReservacion(Date fechaReservacion) {
		this.fechaReservacion = fechaReservacion;
	}
	public Date getChecking() {
		return checking;
	}
	public void setChecking(Date checking) {
		this.checking = checking;
	}
	public Date getCheckout() {
		return checkout;
	}
	public void setCheckout(Date checkout) {
		this.checkout = checkout;
	}
	
	public Integer getCantidadHuespedes() {
		return cantidadHuespedes;
	}
	public void setCantidadHuespedes(Integer cantidadHuespedes) {
		this.cantidadHuespedes = cantidadHuespedes;
	}

	public Integer getEstadoReservacion() {
		return estadoReservacion;
	}

	public void setEstadoReservacion(Integer estadoReservacion) {
		this.estadoReservacion = estadoReservacion;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdHabitacion() {
		return idHabitacion;
	}

	public void setIdHabitacion(Integer idHabitacion) {
		this.idHabitacion = idHabitacion;
	}
	
}
