package pe.edu.unmsm.dyp.hotel.application.dto;

import java.io.Serializable;

import pe.edu.unmsm.dyp.hotel.domain.model.TipoHabitacion;

/**
 * 
 * @author jesus matos
 * @version 1.0 
 * 
 *  * */
public class HabitacionDto implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String  numeroHabitacion;
	private Integer capacidad;
	private boolean disponible;
	private Integer estado;
	private TipoHabitacion tipoHabitacion;
	private String urlFoto;
	private String detalles;
	
	public HabitacionDto() { }
	
	public HabitacionDto(Integer id, String numeroHabitacion, Integer capacidad, boolean disponible, Integer estado,
			TipoHabitacion tipoHabitacion, String urlFoto, String detalles) {
		super();
		this.id = id;
		this.numeroHabitacion = numeroHabitacion;
		this.capacidad = capacidad;
		this.disponible = disponible;
		this.estado = estado;
		this.tipoHabitacion = tipoHabitacion;
		this.urlFoto = urlFoto;
		this.detalles = detalles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumeroHabitacion() {
		return numeroHabitacion;
	}

	public void setNumeroHabitacion(String numeroHabitacion) {
		this.numeroHabitacion = numeroHabitacion;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public TipoHabitacion getTipoHabitacion() {
		return tipoHabitacion;
	}

	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public String getDetalles() {
		return detalles;
	}

	public void setDetalles(String detalles) {
		this.detalles = detalles;
	}
	
	
	
	
}
